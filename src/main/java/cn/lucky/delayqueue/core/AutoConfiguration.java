package cn.lucky.delayqueue.core;

import cn.lucky.delayqueue.common.persistent.action.DelayMsgDel;
import cn.lucky.delayqueue.common.persistent.action.DelayMsgInsert;
import cn.lucky.delayqueue.common.persistent.action.DelayMsgQuery;
import cn.lucky.delayqueue.common.persistent.action.DelayMsgUpdate;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.context.ConfigurationPropertiesAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * listener 配置
 * @author Lucky Pan
 * @date 2022/4/18 10:43
 */
@EnableConfigurationProperties({DelayQueueProperties.class})
@AutoConfigureAfter({ConfigurationPropertiesAutoConfiguration.class})
public class AutoConfiguration {
    public AutoConfiguration() {
    }

    @Bean
    public DelayMsgDel delayMsgDel(JdbcTemplate jdbcTemplate,
                                   DelayQueueProperties properties) {
        return new DelayMsgDel(jdbcTemplate, properties);
    }

    @Bean
    public DelayMsgInsert delayMsgInsert(JdbcTemplate jdbcTemplate) {
        return new DelayMsgInsert(jdbcTemplate);
    }

    @Bean
    public DelayMsgQuery delayMsgQuery(JdbcTemplate jdbcTemplate) {
        return new DelayMsgQuery(jdbcTemplate);
    }

    @Bean
    public DelayMsgUpdate delayMsgUpdate(JdbcTemplate jdbcTemplate) {
        return new DelayMsgUpdate(jdbcTemplate);
    }

}
