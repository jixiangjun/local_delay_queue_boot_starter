package cn.lucky.delayqueue.core;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONUtil;
import cn.lucky.delayqueue.common.anno.DelayQue;
import cn.lucky.delayqueue.common.persistent.DelayMsgEntity;
import cn.lucky.delayqueue.common.persistent.action.DelayMsgDel;
import cn.lucky.delayqueue.common.persistent.action.DelayMsgInsert;
import cn.lucky.delayqueue.common.persistent.action.DelayMsgQuery;
import cn.lucky.delayqueue.common.persistent.action.DelayMsgUpdate;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import javax.annotation.PreDestroy;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.DelayQueue;

/**
 * <p>
 *  延迟队列初始化处理器
 *  自动装配
 * </p>
 *
 * @author pan
 * 2022/10/22 10:07
 */
@Slf4j
public class DelayQueueInitProcessor implements BeanPostProcessor {
    /**
     * 缓存以填充的延时队列
     * 事实上程序中不应该出现对一种消息消费的多处定义
     */
    private static final Map<String,PersistentDelayQueue> MAP = new HashMap<>();
    /**
     * 存放内存中所有的消息的ID，便于批量释放
     */
    private static final ArrayList<Long> MSG_IDS = new ArrayList<>();
    private final DelayMsgQuery msgQuery;
    private final DelayMsgDel msgDel;
    private final DelayMsgInsert msgInsert;
    private final DelayMsgUpdate msgUpdate;

    public DelayQueueInitProcessor(DelayMsgQuery msgQuery,
                                   DelayMsgDel msgDel,
                                   DelayMsgInsert msgInsert,
                                   DelayMsgUpdate msgUpdate) {
        this.msgQuery = msgQuery;
        this.msgDel = msgDel;
        this.msgInsert = msgInsert;
        this.msgUpdate = msgUpdate;
    }

    @SneakyThrows
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        // bean class
        Class<?> clazz = bean.getClass();
        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(DelayQue.class)) {
                field.setAccessible(true);
                Class<?> aClass = field.getAnnotation(DelayQue.class).value();
                String className = aClass.getName();
                if (MAP.containsKey(className)) {
                    log.error("可能的 ❎：【"+className+"】 延迟消息定义了多处消费，建议只定义一处消费！");
                    field.set(bean, MAP.get(className));
                    continue;
                }
                PersistentDelayQueue delayQueue = new PersistentDelayQueue(new DelayQueue<>(), msgInsert, msgDel, msgUpdate);
                List<DelayMsgEntity> msgEntities = msgQuery.queryByMsgType(aClass);
                if (CollUtil.isEmpty(msgEntities)) {
                    field.set(bean, delayQueue);
                    MAP.put(className,delayQueue);
                    continue;
                }
                for (DelayMsgEntity entity : msgEntities) {
                    // 当成功改变了 load_status 才能添加到内存队列中，否者直接 continue
                    // 防止多实例重复执行
                    if (!msgUpdate.loadedStatusById(entity.getId())) {
                        continue;
                    }
                    String jsonStr = JSONUtil.toJsonStr(entity.getMsgJson());
                    Object o = JSONUtil.toBean(jsonStr, aClass);
                    BaseDelayMsg baseDelayMsg = (BaseDelayMsg) o;
                    // 便于根据 ID 删除 数据库数据
                    baseDelayMsg.setId(entity.getId());
                    MSG_IDS.add(entity.getId());
                    delayQueue.initAdd(baseDelayMsg);
                }
                field.set(bean, delayQueue);
                MAP.put(className,delayQueue);
            }
        }
        return bean;
    }

    /**
     * 容器销毁前释放
     */
    @PreDestroy
    public void autoReleaseLoaded() {
        if (CollUtil.isEmpty(MSG_IDS)) {
            return;
        }
        msgUpdate.batchReleaseLoadedStatusById(MSG_IDS);
    }
}
