package cn.lucky.delayqueue.core;

import cn.lucky.delayqueue.common.persistent.action.DelayMsgDel;
import cn.lucky.delayqueue.common.persistent.action.DelayMsgInsert;
import cn.lucky.delayqueue.common.persistent.action.DelayMsgUpdate;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.DelayQueue;

/**
 * <p>
 *  对jdk delay_queue的包装
 * </p>
 *
 * @author pan
 * @date 2022/10/22 16:35
 */
@Slf4j
public class PersistentDelayQueue {
    /**
     * 延迟消息存放
     */
    @Getter
    private DelayQueue<BaseDelayMsg> delayQueue;
    private DelayMsgInsert delayMsgInsert;
    private DelayMsgDel delayMsgDel;
    private DelayMsgUpdate delayMsgUpdate;

    public PersistentDelayQueue() {
    }

    public PersistentDelayQueue(DelayQueue<BaseDelayMsg> delayQueue,
                                DelayMsgInsert msgInsert,
                                DelayMsgDel delayMsgDel,
                                DelayMsgUpdate delayMsgUpdate) {
        this.delayQueue = delayQueue;
        this.delayMsgInsert = msgInsert;
        this.delayMsgDel = delayMsgDel;
        this.delayMsgUpdate = delayMsgUpdate;
    }

    /**
     * 给队列添加延迟消息
     * @param e 延迟消息
     */
    public boolean add(BaseDelayMsg e) {
        // 先持久化数据库，然后添加
        if (!delayMsgInsert.insert(e)) {
            String err = "延迟消息持久化失败！";
            log.error(err + ":{}", e);
            throw new RuntimeException("延迟消息持久化失败！");
        }
        return delayQueue.add(e);
    }

    /**
     * 容器初始化的时候队列添加从数据库加载进来的延迟消息
     * 此方法框架内部使用，外部业务方添加应该使用 {@link #add(BaseDelayMsg)}
     * @param e 延迟消息
     * @return boolean
     */
    protected boolean initAdd(BaseDelayMsg e) {
        return delayQueue.add(e);
    }

    /**
     * 获取延迟消息
     * @return BaseDelayMsg
     */
    @SneakyThrows
    public BaseDelayMsg take() {
        return this.delayQueue.take();
    }

    /**
     * 对延迟消息进行消费确认
     * 如果消息在业务方消费成功，需要手动调用，删除数据库的消息
     * @param e 延迟消息
     * @return boolean 是否删除成功
     */
    public boolean ack(BaseDelayMsg e) {
        // 删除数据库存储的延迟消息
        return delayMsgDel.delMsgById(e.getId());
    }

    /**
     * 当消息被成功置为 {@link cn.lucky.delayqueue.common.enums.LoadStatusEnum#loaded},在接下来的
     * 业务处理过程中处理失败的时候，需要释放 已加载状态；
     * 此方法与上面的 {@link PersistentDelayQueue#ack(BaseDelayMsg)}方法互斥，二者不能同时调用
     * @param e 延迟消息
     */
    public void releaseLoadedStatus(BaseDelayMsg e) {
        delayMsgUpdate.releaseLoadedStatusById(e.getId());
    }
}
