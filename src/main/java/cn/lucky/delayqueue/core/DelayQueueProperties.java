package cn.lucky.delayqueue.core;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 自定义配置
 * @author Lucky Pan
 * 2022/4/17 16:30
 */
@Data
@ConfigurationProperties(DelayQueueProperties.PREFIX)
public class DelayQueueProperties {
    public static final String PREFIX = "lucky.delayqueue";
    /**
     * 是否逻辑删除
     */
    private boolean enableLogicDel = false;

}
