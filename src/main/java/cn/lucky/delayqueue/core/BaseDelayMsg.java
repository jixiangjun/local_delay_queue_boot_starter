package cn.lucky.delayqueue.core;

import cn.hutool.core.date.DateUtil;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;
/**
 * <p>
 *     延迟消息基础类
 *     所有业务延迟消息都需要继承此类
 * </p>
 * @author  pan
 * @date  2022/10/22 11:19
 **/
@Slf4j
@Data
@NoArgsConstructor
public  class BaseDelayMsg implements Delayed, Serializable {
    /**
     * 触发时间(失效时间点)
     */
    private long time;
    /**
     * 对应数据库中的存储ID，存储的时候不会存入，会在容器初始化的时候带入
     * 用于ack 之后的ID 删除
     */
    private Long id;

    private TimeUnit unit;

    public BaseDelayMsg(Long time, TimeUnit unit) {
        if (
        Objects.isNull(time) ||
        Objects.isNull(unit)) {
            String err = "延迟消息构建缺失参数";
            log.error(err + ": 【{},{},{}】",id,time,unit);
            throw new RuntimeException(err);
        }
        this.time = System.currentTimeMillis() + (time > 0 ? unit.toMillis(time) : 0);
        this.unit = unit;
    }

    /**
     * 返回剩余有效时间
     * @param unit the time unit
     */
    @Override
    public long getDelay(TimeUnit unit) {
        return unit.convert(time - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
    }

    /**
     * 比较两个Delayed对象的大小, 比较顺序如下:
     * 比较失效时间点, 先失效的返回-1,后失效的返回1
     */
    @Override
    public int compareTo(Delayed o) {
        BaseDelayMsg item = (BaseDelayMsg) o;
        return this.time - item.time <= 0 ? -1 : 1;
    }

    @Override
    public String toString() {
        return "{" + "time=" + DateUtil.formatDateTime(new Date(time)) + ", id='" + id + '\'' + '}';
    }

}