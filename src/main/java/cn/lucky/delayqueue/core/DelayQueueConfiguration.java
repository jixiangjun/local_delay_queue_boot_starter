package cn.lucky.delayqueue.core;

import cn.lucky.delayqueue.common.persistent.action.DelayMsgDel;
import cn.lucky.delayqueue.common.persistent.action.DelayMsgInsert;
import cn.lucky.delayqueue.common.persistent.action.DelayMsgQuery;
import cn.lucky.delayqueue.common.persistent.action.DelayMsgUpdate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;

/**
 * <p>
 *  延迟队列注入处理
 * </p>
 *
 * @author pan
 * @date 2022/10/23 13:54
 */
public class DelayQueueConfiguration {
    @Bean
    public DelayQueueInitProcessor delayQueueProcessor(@Lazy DelayMsgQuery delayMsgQuery,
                                                       @Lazy DelayMsgDel delayMsgDel,
                                                       @Lazy DelayMsgInsert delayMsgInsert,
                                                       @Lazy DelayMsgUpdate delayMsgUpdate) {
        return new DelayQueueInitProcessor(delayMsgQuery, delayMsgDel, delayMsgInsert, delayMsgUpdate);
    }
}
