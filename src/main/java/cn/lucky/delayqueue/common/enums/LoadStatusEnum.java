package cn.lucky.delayqueue.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * <p>
 *  数据库延迟消息加载状态
 * </p>
 *
 * @author pan
 * 2022/10/23 09:48
 */
@AllArgsConstructor
@Getter
public enum LoadStatusEnum {
    /**
     * 未被加载 到内存队列
     */
    no_loaded("0", "未被加载"),
    /**
     * 已被加载 到内存队列
     */
    loaded("1", "已被加载");

    private final String code;
    private final String label;

    public static LoadStatusEnum findByValue(String code){
        return Arrays.stream(LoadStatusEnum.values())
                .filter(item -> item.getCode().equals(code))
                .findFirst()
                .orElse(null);
    }
}
