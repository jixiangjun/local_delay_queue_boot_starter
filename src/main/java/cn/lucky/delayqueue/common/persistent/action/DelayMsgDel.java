package cn.lucky.delayqueue.common.persistent.action;

import cn.lucky.delayqueue.common.persistent.DelayMsgEntityRowMapper;
import cn.lucky.delayqueue.core.DelayQueueProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * <p>
 * 延迟消息数据库删除
 * </p>
 *
 * @author pan
 * @date 2022/10/22 12:02
 */
@Slf4j
public class DelayMsgDel {
    private final JdbcTemplate jdbcTemplate;
    private final DelayQueueProperties properties;

    public DelayMsgDel(JdbcTemplate jdbcTemplate,
                       DelayQueueProperties properties) {
        this.jdbcTemplate = jdbcTemplate;
        this.properties = properties;
    }

    /**
     * 根据ID 删除延迟消息
     * @param msgId 延迟消息ID主键
     * @return boolean
     */
    public boolean delMsgById(Long msgId) {
        if (properties.isEnableLogicDel()) {
            return delMsgByIdLogic(msgId);
        }
        String sql = " delete FROM "+ DelayMsgEntityRowMapper.DELAY_MSG_TABLE +" where "+ DelayMsgEntityRowMapper.ID +" = ? ";
        int update = jdbcTemplate.update(sql, msgId);
        log.info("删除延迟消息：【{}】", msgId);
        return update > 0;
    }

    /**
     * 逻辑删除
     * @param msgId 延迟消息ID主键
     * @return boolean
     */
    private boolean delMsgByIdLogic(Long msgId) {
        String sql = "UPDATE "+DelayMsgEntityRowMapper.DELAY_MSG_TABLE+" set "+DelayMsgEntityRowMapper.IS_DEL+" = 1 WHERE id  = ?";
        int update = jdbcTemplate.update(sql, msgId);
        log.info("逻辑删除延迟消息：【{}】", msgId);
        return update > 0;
    }

}
