package cn.lucky.delayqueue.common.persistent.action;

import cn.lucky.delayqueue.common.persistent.DelayMsgEntity;
import cn.lucky.delayqueue.common.persistent.DelayMsgEntityRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

/**
 * <p>
 * 延迟消息数据库查询
 * </p>
 *
 * @author pan
 * @date 2022/10/22 12:02
 */
public class DelayMsgQuery {
    private final JdbcTemplate jdbcTemplate;

    public DelayMsgQuery(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<DelayMsgEntity> queryByMsgType(Class msgClazz) {
        String sql = " select * from "+ DelayMsgEntityRowMapper.DELAY_MSG_TABLE +
                " where "+DelayMsgEntityRowMapper.MSG_CLAZZ+" = ? ";
        return jdbcTemplate.query(logicDelProcess(sql), new DelayMsgEntityRowMapper(), msgClazz.getName());
    }

    public List<DelayMsgEntity> query() {
        String sql = " select * from "+ DelayMsgEntityRowMapper.DELAY_MSG_TABLE;
        return jdbcTemplate.query(logicDelProcess(sql), new DelayMsgEntityRowMapper());
    }

    /**
     * 无需条件判断，直接查 未逻辑删除的
     * @param sql SQL
     * @return 逻辑处理后SQL
     */
    private String logicDelProcess(String sql) {
        sql = sql + " and "+ DelayMsgEntityRowMapper.IS_DEL+" = 0";
        return sql;
    }


}
