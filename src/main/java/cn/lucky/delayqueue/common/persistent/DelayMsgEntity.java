package cn.lucky.delayqueue.common.persistent;

import lombok.Data;

import java.util.Date;

/**
 * <p>
 *  延迟消息数据库实体
 * </p>
 *
 * @author pan
 * @date 2022/10/22 12:08
 */
@Data
public class DelayMsgEntity {
    /**
     * ID
     */
    private Long id;
    /**
     * 延迟消息 类 全限定名
     */
    private String msgClazz;
    /**
     * 延迟消息 序列化JSON 串，用于反序列化延迟消息
     */
    private String msgJson;

    /**
     * 延迟消息是否被加载到了内存队列
     * {@link cn.lucky.delayqueue.common.enums.LoadStatusEnum}
     */
    private String loadStatus;

    private Date createTime;

    /**
     * 逻辑删除标识
     */
    private Integer isDel;
}
