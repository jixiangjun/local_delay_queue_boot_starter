package cn.lucky.delayqueue.common.persistent;

import lombok.NoArgsConstructor;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * <p>
 *      延迟消息数据库实体转换器
 * </p>
 *
 * @author pan
 * @date 2022/10/22 12:30
 */
@NoArgsConstructor
public class DelayMsgEntityRowMapper implements RowMapper<DelayMsgEntity> {
    /**
     * 延迟消息数据库表名
     */
    public static final String DELAY_MSG_TABLE = "delay_message";
    /**
     * 延迟消息表字段
     */
    public static final String ID = "id";
    public static final String MSG_CLAZZ = "msg_clazz";
    public static final String MSG_JSON = "msg_json";
    public static final String LOAD_STATUS = "load_status";
    public static final String CREATE_TIME = "create_time";
    public static final String IS_DEL = "is_del";

    @Override
    public DelayMsgEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
        DelayMsgEntity entity = new DelayMsgEntity();
        entity.setId(rs.getLong(ID));
        entity.setMsgClazz(rs.getString(MSG_CLAZZ));
        entity.setMsgJson(rs.getString(MSG_JSON));
        entity.setLoadStatus(rs.getString(LOAD_STATUS));
        entity.setCreateTime(rs.getDate(CREATE_TIME));
        entity.setIsDel(rs.getInt(IS_DEL));
        return entity;
    }
}
