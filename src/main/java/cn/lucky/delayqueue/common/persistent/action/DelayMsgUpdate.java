package cn.lucky.delayqueue.common.persistent.action;

import cn.lucky.delayqueue.common.enums.LoadStatusEnum;
import cn.lucky.delayqueue.common.persistent.DelayMsgEntityRowMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 延迟消息数据库修改
 * </p>
 *
 * @author pan
 * @date 2022/10/23 09:59
 */
@Slf4j
public class DelayMsgUpdate {
    private final JdbcTemplate jdbcTemplate;

    public DelayMsgUpdate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * 更新 LoadStatus
     * @param msgId 延迟消息ID主键
     * @return boolean
     */
    public boolean loadedStatusById(Long msgId) {
        String sql = "UPDATE "+DelayMsgEntityRowMapper.DELAY_MSG_TABLE +
                " set "+DelayMsgEntityRowMapper.LOAD_STATUS+" = " + LoadStatusEnum.loaded.getCode() +
                " WHERE id  = ? and " +
                DelayMsgEntityRowMapper.LOAD_STATUS+" = '"+LoadStatusEnum.no_loaded.getCode()+"'";
        int update = jdbcTemplate.update(sql, msgId);
        log.info("更新【已加载】延迟消息：【{}】", msgId);
        return update > 0;
    }

    /**
     * 释放已加载为 未加载
     * @param msgId 延迟消息ID主键
     */
    public void releaseLoadedStatusById(Long msgId) {
        String sql = "UPDATE "+DelayMsgEntityRowMapper.DELAY_MSG_TABLE +
                " set "+DelayMsgEntityRowMapper.LOAD_STATUS+" = " + LoadStatusEnum.no_loaded.getCode() +
                " WHERE id  = ? and " +
                DelayMsgEntityRowMapper.LOAD_STATUS+" = '"+LoadStatusEnum.loaded.getCode()+"'";
        jdbcTemplate.update(sql, msgId);
        log.info("手动【释放已加载】延迟消息：【{}】", msgId);
    }

    /**
     * 根据ID 批量释放已加载消息
     * @param ids 消息ID list
     */
    public void batchReleaseLoadedStatusById(List<Long> ids) {
        // 条件： load_status 是已释放的。并且是未逻辑删除的
        String sql = "UPDATE "+DelayMsgEntityRowMapper.DELAY_MSG_TABLE +
                " set "+DelayMsgEntityRowMapper.LOAD_STATUS+" = " + LoadStatusEnum.no_loaded.getCode() +
                " WHERE id in(:ids) and " +
                DelayMsgEntityRowMapper.LOAD_STATUS+" = '"+LoadStatusEnum.loaded.getCode()+"'" +
                " and "+DelayMsgEntityRowMapper.IS_DEL+" = '0'";

        Map<String, Object> args  = new HashMap<>();
        args.put("ids", ids);
        NamedParameterJdbcTemplate givenParamJdbcTemp = new NamedParameterJdbcTemplate(jdbcTemplate);
        int releaseNum = givenParamJdbcTemp.update(sql, args);
        log.info("shutdown：【释放已加载】延迟消息：【{}】条", releaseNum);
    }

}
