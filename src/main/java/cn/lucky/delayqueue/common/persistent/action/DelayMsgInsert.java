package cn.lucky.delayqueue.common.persistent.action;

import cn.hutool.json.JSONUtil;
import cn.lucky.delayqueue.common.persistent.DelayMsgEntityRowMapper;
import cn.lucky.delayqueue.core.BaseDelayMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * <p>
 * 延迟消息数据库新增
 * </p>
 *
 * @author pan
 * @date 2022/10/22 12:02
 */
@Slf4j
public class DelayMsgInsert {
    private final JdbcTemplate jdbcTemplate;

    public DelayMsgInsert(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * 数据库插入延迟消息
     * @param delayMsg 延迟消息
     * @return boolean
     */
    public boolean insert(BaseDelayMsg delayMsg) {
        String sql = " insert into "+ DelayMsgEntityRowMapper.DELAY_MSG_TABLE +
                "("+DelayMsgEntityRowMapper.MSG_CLAZZ+", "+DelayMsgEntityRowMapper.MSG_JSON +
                ") values(?,?)";
        int update = jdbcTemplate.update(sql, delayMsg.getClass().getName(), JSONUtil.toJsonStr(delayMsg));
        log.info("延迟消息新增：【{}】", delayMsg);
        return update > 0;
    }

}
