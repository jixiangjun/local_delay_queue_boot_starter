package cn.lucky.delayqueue.common.anno;

import java.lang.annotation.*;

/**
 * <p>
 * 延迟队列注解
 * </p>
 *
 * @author pan
 * 2022/10/22 09:56
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface DelayQue {
    /**
     * 队列消息 clazz类型，必填
     */
    Class<?> value();
}
